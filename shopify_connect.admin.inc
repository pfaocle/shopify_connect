<?php

/**
 * @file
 * Admin page callback file for the shopify_connect module.
 *
 */


/**
 * Status page
 */
function shopify_connect_admin_status() {
  try {
    $storeConnection = new ShopifyStoreConnection();
  }
  catch (Exception $e) {
    $output = $e;
    return t('Not connected. Details:<br /><pre>' . $e . '</pre>');
  }

  $output  = '<p>' . t('Connected to @storename at @storeurl', array('@storename' => $storeConnection->store_info['name'], '@storeurl' => $storeConnection->store_info['domain'])) . '</p>';
  $output .= '<p>' . $storeConnection->storeAPICallLimitMessage(array()) . '</p>';

  // Print some debug information if mode is set.
  if (variable_get('shopify_connect_debug', 0) > 1) {
    $output .= '<p><strong><em>__toString()</em></strong></p>' . (string) $storeConnection;                     // toString
    $output .= '<p><strong><em>as Object</em></strong></p><pre>' . print_r($storeConnection, TRUE) . '</pre>';  // Entire object
  }

  return $output;
}


/**
 * Settings form
 */
function shopify_connect_admin_settings() {
  $form = array();

  $form['store_connection'] = array(
    '#type' => 'fieldset',
    '#title' => 'Store private application details',
  );
  $form['store_connection']['shopify_connect_store_domain'] = array(
    '#type' => 'textfield',
    '#title' => t('Store URL'),
    '#default_value' => variable_get('shopify_connect_store_domain', ''),
    '#description' => t("Your store's domain, for example <em>store.myshopify.com</em>"),
  );
  $form['store_connection']['shopify_connect_api_key'] = array(
    '#type' => 'textfield',
    '#title' => t('Store API key'),
    '#default_value' => variable_get('shopify_connect_api_key', ''),
    '#description' => t('Store API key.'),
  );
  $form['store_connection']['shopify_connect_password'] = array(
    '#type' => 'password',
    '#title' => t('Store password'),
    '#description' => t("Your store's password. <strong>Warning:</strong> this password is stored as plain text."),
  );

  $form['store_options'] = array(
    '#type' => 'fieldset',
    '#title' => 'Options',
  );
  $form['store_options']['shopify_connect_debug'] = array(
    '#type' => 'select',
    '#title' => 'Debug level',
    '#options' => array(
      0 => t('None'),
      1 => t('Minimal'),
      2 => t('Verbose'),
    ),
    '#default_value' => variable_get('shopify_connect_debug', 0),
    '#description' => t("Set the debugging level - controls how much debug output is displayed."),
  );

  return system_settings_form($form);
}

function shopify_connect_admin_settings_validate($form, &$form_state) {
  if (!_shopify_connect_is_valid_shopify_store_domain($form_state['values']['shopify_connect_store_domain'])) {
    form_set_error('shopify_connect_store_domain', t('Not a valid domain name.'));
  }
}
