<?php

/**
 * @file
 * Admin page callback file for the shopify_connect_store_admin module.
 *
 */


/**
 * Main configuration page.
 */
function shopify_connect_store_admin_settings() {
  $form = array();

  $form['shopify_connect_store_admin_product_types'] = array(
    '#type' => 'textarea',
    '#title' => t('Product types'),
    '#default_value' =>  variable_get('shopify_connect_store_admin_product_types', ''),
    '#description' => t("Enter your store's defined product types, one per line. <em>Product types are unavailable via the Shopify API</em>."),
  );

  return system_settings_form($form);
}
