<?php

/**
 * @file
 * Provides some administrative lists of Shopify store objects via the API.
 *
 * Warning, possibly crummy.
 */


/**
 * Denotes the maximum number of items retrievable via API.
 *
 * The Shopify API enforces a maximum number of items retrieved for items
 * such as Products, Articles, CustomCollections etc.
 */
define('SHOPIFY_CONNECT_API_MAX_ITEMS', 250);


/**
 * Implements hook_menu().
 */
function shopify_connect_store_admin_menu() {
  $items = array();

  $items['admin/shopify/products'] = array(
    'title' => 'Products',
    'description' => 'Administer products by collection.',
    'page callback' => 'shopify_connect_store_admin_overview',
    'page arguments' => array('collection'),
    'access arguments' => array('administer shopify stores'),
    'type' => MENU_NORMAL_ITEM,
    'weight' => 0,
  );
  $items['admin/shopify/products/collection'] = array(
    'title' => 'By collection',
    'type' => MENU_DEFAULT_LOCAL_TASK,
  );
  $items['admin/shopify/products/type'] = array(
    'title' => 'By type',
    'description' => 'Administer products by type.',
    'page callback' => 'shopify_connect_store_admin_overview',
    'page arguments' => array('type'),
    'access arguments' => array('administer shopify stores'),
    'type' => MENU_LOCAL_TASK,
    'weight' => 1,
  );
  $items['admin/shopify/products/all'] = array(
    'title' => 'All',
    'description' => 'Administer all products.',
    'page callback' => 'shopify_connect_store_admin_overview',
    'page arguments' => array('all'),
    'access arguments' => array('administer shopify stores'),
    'type' => MENU_LOCAL_TASK,
    'weight' => 2,
  );

  // Configuration page.
  $items['admin/config/shopify/store'] = array(
    'title' => 'Shopify store admin configuration',
    'description' => 'Shopify store admin configuration.',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('shopify_connect_store_admin_settings'),
    'access arguments' => array('administer shopify stores'),
    'file' => 'shopify_connect_store_admin.admin.inc',
  );

  return $items;
}


/**
 * Handle main listings pages.
 *
 * @param String $callback_arg - one of collection, type or all
 *
 */
function shopify_connect_store_admin_overview($callback_arg) {
  switch ($callback_arg) {
    case 'collection':
      // collections_filter
      $build['collections_filter'] = drupal_get_form('shopify_connect_store_admin_collections_filter');
      break;
    case 'type':
      // types_filter
      $build['types_filter'] = drupal_get_form('shopify_connect_store_admin_types_filter');
      break;
  }

  $build['products_list'] = drupal_get_form('shopify_connect_store_admin_products', $callback_arg);
  return $build;
}


function shopify_connect_store_admin_collections_filter() {
  $collection_id = isset($_SESSION['shopify_connect_store_admin_collectionid']) ? $_SESSION['shopify_connect_store_admin_collectionid'] : 0;

  $ds = new ShopifyStoreConnection();

  $form = array();
  $form['collections'] = array(
    '#type' => 'fieldset',
    '#title' => t('Collection'),
    '#attributes' => array('class' => array('container-inline')),
  );
  $form['collections']['collection_select'] = array(
    '#type' => 'select',
    '#options' => $ds->storeCollectionsSelect(),
    '#default_value' => $collection_id,
  );
  $form['collections']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Submit'),
  );
  $form['collections']['reset'] = array(
    '#type' => 'submit',
    '#value' => t('Reset'),
  );
  return $form;
}

function shopify_connect_store_admin_collections_filter_submit($form, &$form_state) {
  if ($form_state['clicked_button']['#id'] == 'edit-submit') {
    $_SESSION['shopify_connect_store_admin_collectionid'] = intval($form_state['input']['collection_select']);
  }
  elseif ($form_state['clicked_button']['#id'] == 'edit-reset') {
    $_SESSION['shopify_connect_store_admin_collectionid'] = 0;
  }
}


function shopify_connect_store_admin_types_filter() {
  $product_type = isset($_SESSION['shopify_connect_store_admin_producttype']) ? $_SESSION['shopify_connect_store_admin_producttype'] : '';

  $form = array();
  $form['types'] = array(
    '#type' => 'fieldset',
    '#title' => t('Product type'),
    '#attributes' => array('class' => array('container-inline')),
  );
  $form['types']['type_select'] = array(
    '#type' => 'select',
    '#options' => _shopify_connect_store_admin_get_product_types(),
    '#default_value' => $product_type,
  );
  $form['types']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Submit'),
  );
  $form['types']['reset'] = array(
    '#type' => 'submit',
    '#value' => t('Reset'),
  );
  return $form;
}

function shopify_connect_store_admin_types_filter_submit($form, &$form_state) {
  if ($form_state['clicked_button']['#id'] == 'edit-submit') {
    $_SESSION['shopify_connect_store_admin_producttype'] = $form_state['input']['type_select'];
  }
  elseif ($form_state['clicked_button']['#id'] == 'edit-reset') {
    $_SESSION['shopify_connect_store_admin_producttype'] = '';
  }
}


/**
 * List of products form.
 */
function shopify_connect_store_admin_products($form, &$form_state) {
  drupal_add_css(drupal_get_path('module', 'shopify_connect_store_admin') . '/shopify_connect_store_admin.css');

  $filter_arg = $form_state['build_info']['args'][0];

  if ($filter_arg == 'collection') {
    $collection_id = isset($_SESSION['shopify_connect_store_admin_collectionid']) ? $_SESSION['shopify_connect_store_admin_collectionid'] : 0;
  }
  elseif ($filter_arg == 'type') {
    $product_type_id = isset($_SESSION['shopify_connect_store_admin_producttype']) ? $_SESSION['shopify_connect_store_admin_producttype'] : '';
    $types = _shopify_connect_store_admin_get_product_types();
    $product_type = $types[$product_type_id];
  }

  // Horrible if logic to ensure we have a parameter for each type of filter before proceeding
  if ($filter_arg == 'all' || ($filter_arg == 'collection' && $collection_id) || ($filter_arg == 'type' && $product_type)) {
    $header = array(
      'title' => t('Title'),
      'product_type' =>t('Type'),
      'tags' => t('Tags'),
      'date' => t('Updated'),
      'operations' => t('Operations'),
    );

    $form['options'] = array(
      '#type' => 'fieldset',
      '#title' => t('Update options'),
      '#attributes' => array('class' => array('container-inline')),
    );
    $options = array();
    foreach (module_invoke_all('shopify_product_operations') as $operation => $array) {
      $options[$operation] = $array['label'];
    }
    $form['options']['operation'] = array(
      '#type' => 'select',
      '#title' => t('Operation'),
      '#title_display' => 'invisible',
      '#options' => $options,
      '#default_value' => 'unblock',
    );
    $options = array();
    $form['options']['submit'] = array(
      '#type' => 'submit',
      '#value' => t('Update'),
    );

    $ds = new ShopifyStoreConnection();
    switch ($filter_arg) {
      case 'collection':
        $response = $ds->storeAPICall('GET', '/admin/products.json?collection_id=' . $collection_id . '&limit=' . SHOPIFY_CONNECT_API_MAX_ITEMS);
        break;
      case 'type':
        $response = $ds->storeAPICall('GET', '/admin/products.json?product_type=' . urlencode($product_type) . '&limit=' . SHOPIFY_CONNECT_API_MAX_ITEMS);
        break;
      case 'all':
        $response = $ds->storeAPICall('GET', '/admin/products.json?limit=' . SHOPIFY_CONNECT_API_MAX_ITEMS);
        break;
    }

    $options = array();
    foreach ($response as $product) {
      $sDate = new DateTime($product['updated_at']);
      $options[$product['id']] = array(
        'title' => array(
          'data' => array(
            '#type' => 'link',
            '#title' => $product['title'],
            '#href' => $ds->baseURL . $ds->storeGetPath($product['handle'], 'product'),
            '#options' => array('attributes' => array('title' => t('[id:@id] View in store.', array('@id' => $product['id'])))),
          ),
        ),
        'product_type' => $product['product_type'],
        'tags' => $product['tags'],
        'date' => format_date($sDate->format('U'), 'short'),
        'operations' => l(('edit'), $ds->baseURL . '/admin/products/' . $product['id']),
      );
    }

    $form['products'] = array(
      '#type' => 'tableselect',
      '#header' => $header,
      '#options' => $options,
      '#empty' => t('No products available.'),
      '#attributes' => array('id' => 'shopify-products-list'),
    );
    $form['pager'] = array('#markup' => theme('pager'));
  }
  else {
    $form['products'] = array('#markup' => t('Select a !filter_type_name.', array('!filter_type_name' => $filter_arg)));
  }
  return $form;
}

function shopify_connect_store_admin_products_validate($form, &$form_state) {
  $form_state['values']['products'] = array_filter($form_state['values']['products']);
  if (count($form_state['values']['products']) == 0) {
    form_set_error('', t('No products selected.'));
  }
}

/**
 * Submit the products administration update form.
 */
function shopify_connect_store_admin_products_submit($form, &$form_state) {
  $operations = module_invoke_all('shopify_product_operations', $form, $form_state);
  $operation = $operations[$form_state['values']['operation']];
  // Filter out unchecked accounts.
  $products = array_filter($form_state['values']['products']);
  if ($function = $operation['callback']) {
    // Add in callback arguments if present.
    if (isset($operation['callback arguments'])) {
      $args = array_merge(array($products), $operation['callback arguments']);
    }
    else {
      $args = array($products);
    }
    call_user_func_array($function, $args);

    drupal_set_message(t('The update has been performed.'));
  }
}


/**
 * Returns a non-associative array of product type strings, as defined in settings.
 */
function _shopify_connect_store_admin_get_product_types() {
  $product_types = array();
  $product_types_var = variable_get('shopify_connect_store_admin_product_types', '');
  if (!empty($product_types_var)) {
    $product_types = explode("\r\n", $product_types_var);
  }
  return $product_types;
}


/*********************************************************************************************
 * Product bulk operations
 *********************************************************************************************/

/**
 *
 */
function shopify_connect_shopify_product_operations($form = array(), $form_state = array()) {
  $operations = array(
    'delete' => array(
      'label' => t('Delete the selected products'),
      'callback' => 'shopify_connect_store_admin_delete',
    ),
    'stripbreaks' => array(
      'label' => t('Strip <br /> tags from body'),
      'callback' => 'shopify_connect_store_admin_stripbreaks',
    ),
    'forceolddates' => array(
      'label' => t('Force dates to old date'),
      'callback' => 'shopify_connect_store_admin_forceolddates',
    ),
  );
  return $operations;
}


/**
 * Callback function for admin mass product delete.
 */
function shopify_connect_store_admin_delete($products) {
  drupal_set_message(t('DEBUG/NOT FUNCTIONAL - deleting @ids', array('@ids' => implode(', ', $products))));

  //var_dump($products); exit;
  //dpm($products);
}


/**
 * Callback function for admin mass tag stripping.
 */
function shopify_connect_store_admin_stripbreaks($products) {
  $ds = new ShopifyStoreConnection();
  foreach ($products as $id => $product) {

    $r = $ds->storeAPICall('GET', "/admin/products/{$id}.json");

    // Make the product update.
    $params = array(
      'product' => array(
        'id' => $id,
        'body' => str_replace('<br />', ' ', $r['body_html']),
      )
    );
    $r = $ds->storeAPICall('PUT', "/admin/products/{$id}.json", $params);
    drupal_set_message(t('Product !title body stripped of break tags', array('!title' => $r['title'])));
  }

  drupal_set_message(t('Removed break tags from @ids', array('@ids' => implode(', ', $products))));
}


function shopify_connect_store_admin_forceolddates($products) {
  $old_date = '2012-09-30T16:09:40+01:00';

  $ds = new ShopifyStoreConnection();
  foreach ($products as $id => $product) {

    //$r = $ds->storeAPICall('GET', "/admin/products/{$id}.json");

    // Make the product update.
    $params = array(
      'product' => array(
        'id' => $id,
        'created_at' => $old_date,
        'updated_at' => $old_date,
        'published_at' => $old_date,
      )
    );
    $r = $ds->storeAPICall('PUT', "/admin/products/{$id}.json", $params);
    dpm($r);
    drupal_set_message(t('Product !title dates set to !date', array('!title' => $r['title'], '!date' => $old_date)));
  }

  drupal_set_message(t('Removed break tags from @ids', array('@ids' => implode(', ', $products))));
}