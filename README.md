# shopify_connect

Provides connections between [Drupal](http://drupal.org/) site and one or more [Shopify](http://www.shopify.com/) stores via [private applications](http://wiki.shopify.com/Private_applications). Intended as a suite of modules for Shopify store managers.


## Requirements

Currently for Drupal 7.x

Depends on the [Libraries API](http://drupal.org/project/libraries) to handle an external library dependency on [shopify.php](http://github.com/sandeepshetty/shopify.php), a lightweight PHP/JSON client for the [Shopify API](http://api.shopify.com/). The shopify.php library only works with legacy authentication and requires PHP 5.3 with [cURL support](http://php.net/manual/en/book.curl.php).


## Installation

1. Ensure that required modules are available - _Libraries API_
2. [Download](http://bitbucket.org/leafish_paul/shopify_connect/downloads) shopify_connect and extract it in the appropriate `modules/shopify_connect/` folder for your site.
3. Download [shopify.php](http://github.com/sandeepshetty/shopify.php) and place it in your `sites/all/libraries/` directory.
4. Enable the module.

Once the module is installed in Drupal, select the roles you would like to allow to configure Shopify store connections at `/admin/people/permissions`.


## Additional modules

* `shopify_connect_store_admin` provides some administrative listings of store objects via the API and allows for bulk operations on them. Note this is _not_ compatible with Views Bulk Operations.


## Limitations and known issues

* shopify.php library only works with legacy authentication
* the Shopify private application password is stored as plain text in a Drupal system variable
